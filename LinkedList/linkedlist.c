# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <stdbool.h>
//----------------------------------------------------
//------------- Linked List Program ------------------
//----------------------------------------------------


// Make a NODE/ block structure: 
// has the data and the reference to the next node (data, link)


//***There is also DOUBLE LINK LIST that link to Previous and next, and there are CIRCULAR LINK LIST that connect to HEAD instead of a NULL.***


struct node {

	int data; // number to be put in
	//int index; // help keep track of position
	struct node *next; //address that points to next node 

};

// Create variable for FIRST EMPTY header

struct node *head = NULL;

struct node *current = NULL;




// create First_NODE

void first_node (int stdata){

	printf("Creating first node....\n");

	struct node* first_ptr = (struct node*) malloc ( sizeof (struct node) );

	//first_ptr-> index = 0;//... or... first_node -> index =0;
	first_ptr -> data = stdata;
	first_ptr -> next = NULL;

	printf ("First node sucessfull.\nValue of First/head node:%d\n", stdata);

	

	if (first_ptr==NULL){ 

		perror("\nError in creating First Node!\n");

		}

	head= current = first_ptr;


}


// Add node to end of list

void add_end (int val){
	//make the new node
	struct node *ptr = (struct node*)malloc (sizeof (struct node)); 

	//connect the node to the previous node made called current
	current -> next = ptr; // fill current with ptr value
	current = ptr ; // copy ptr to current 
	

	// put the in the node and attach the NULL/ end of line to it
	ptr->data = val;
	ptr->next = NULL;
	


}

/*
// Add a node to middle of list

void add_mid (int data, int n){
	
}
*/


// Add a node begginng

void add_beg (int val){

	struct node *ptr= (struct node*) malloc (sizeof (struct node));
	
	ptr -> data = val;

	if (head != NULL){
	ptr -> next = head;
	head = ptr;
	}
	else {first_node (val);}
}



//Delete a Node
void Del_pos (int pos) {

	printf ("\nPreparing to delete value %d from list\n", pos);

	struct node *prev = head;

	struct node *cur= head; // poiint to head

	if (pos==0){ cur = head -> next; free (head); head = cur; return;}
	
	
	for (int i=0 ; i < pos ; i++){ // 
		prev = cur;
		cur = cur->next;
	}
	
	prev -> next = cur->next;
	free (cur);



}
//Delete a Value

void Del_val(int val)
{

	if ((head) -> data == val){
		head = (head) -> next ;
	 
	}
	
	else {
		
		struct node *cur = head, *previous= head;
		
		while (cur -> data != val && cur -> next != NULL){ // go through the link list and search for target
			
			previous= cur; // follow current
			
			cur= cur -> next; // move forward on the list
			
		}
		
		if (cur -> next == NULL){ // we reached the end of the list and element doesnt exist
			
			perror ("Error Occured");
			
		}
		
		previous -> next = cur -> next; // connect the list together
		
		free (cur); // delete the target
		
	}	
	 
	 // new list set

}


//Print the Current List
void Print(){
	// Keep track of train wagon that starts at first list

	struct node *ptr= head;
	printf ("\n------------Printing list--------\n");

	while (ptr != NULL){
			printf("\n [%d] \n",ptr -> data);
			ptr = ptr -> next;
	}
}


//Sort the list in asceding order


//---MAIN--

int main (){
	


	int stdata, val, index, n, i=0, del, delb;
	char d;


	printf ("Welcome to Link List Program V1.0...\n");

for (;;){

//---------First node-----------
	printf("\nPlease enter first node:\n");

	scanf ("%d", &stdata);

	first_node (stdata);




//---------Add node-----------
	printf ("\n Inserting at End of List...\n\nHow many numbers will be added at the end:\n");
	scanf("%d", &n);

	for (i= 0; i<n; i++){

		printf("Value of node:\n");
		scanf ("%d", &val);
		add_end (val);
		Print();
		}

//---------Add node to first -----------
	printf("Inserting beginning of list....\n\n Value of node:");
	scanf("%d",&val);

	add_beg (val);

	Print ();

//--------Add to node position------------

//---------Delete node position-----------
	printf("\nWhich position would you like to delete:\n");
	scanf ("%d", &del);

	Del_pos (del);

	Print ();

	
//-----------Delete node value--------
 
	//printf ("\nWhich value would you like to dele:\n");

	//scanf ("%d",&delb);

	//Del_val (&head, delb);

	//Print ();






	printf("\n---------------------\nProgram complete\n----------------------\n");



}


return 0;

}


