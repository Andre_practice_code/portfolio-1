/* *** 			        semaa.c     			            ***	*/
/* This program demonstrates how to use a semaphore to control the access 	*/
/* of a shared memory block by 2 processes (semaa and semab).          	  	*/
/* Note that the semaphore is implemented with just another shared memory 	*/
/* object, as opposed to using a single shared memory object for both access 	*/
/* control and data.								*/
/* Note also that you have to specify the complete directory path to name    	*/
/* both your shared memory object as well as your semaphore. Use the pwd     	*/
/* command to determine your current working directory. 	         	*/
/* You should inspect the output from these 2 processes carefully so you are 	*/
/* satisfied with what they are supposed to have done.			     	*/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
//#include <process.h>
#include <semaphore.h>
#include <sys/mman.h> 

#define		OFLAGS		O_CREAT|O_RDWR
#define		SIZE		4096
#define		PROT		PROT_READ|PROT_WRITE
#define		MFLAGS		MAP_SHARED

int main( )
{
	sem_t	*sem;
	pid_t	pid; //*** int type which is capable of representing a process ID
	int	shmem, i, s_return;
	char	*memLocation;
	
// Create a shared memory object and report error if there are any

	shmem=shm_open("shared_memory", OFLAGS, 0777); //*** creates or opens POSIX shared memory
	if (shmem == -1)
	{
		printf("shared_memory failed to open...\n");
		fflush(stdout);
	}
	else
	{
		if (ftruncate (shmem,SIZE) == -1)
		{
			printf ("Failed to set size for -- shmem -- \n");
			fflush(stdout);
		}
		else
		{
		//***mmap (address to point to, size of memory, protection, determines if updates to mapping are visible to other mappings, fd , offset)
			memLocation = mmap (0,  SIZE, PROT, MFLAGS, shmem, 0);//*** mmap() creates a new mapping in the virtual address space

			if (memLocation == MAP_FAILED)
			{
				printf (" Failed to map to shared memory...\n");
				fflush(stdout);
			}
		}	
	}	
	
// Open a semaphore similar to the way we open a shared memory object and reset its count value
	sem = sem_open ("shared_sem", O_CREAT, S_IRWXG | S_IRWXO | S_IRWXU, 0);
	//*** is a variable that is used for controlling access to a common resource used by mult processes
	if (sem == (sem_t *)(-1)) 
	{
	   printf ("User: Semaphore failed to open....\n");
	   fflush(stdout);
	}
	strcpy(memLocation, "START");

	pid = fork();//***creates a new process

	//Check process ID
	if(pid > 0){
		printf("I am the original process\n");	
		fflush(stdout);
	}else if(pid == 0){
		printf("I am the new process\n");
		fflush(stdout);
		execl(memLocation, (char*) 0); //***does not return if successful child process, if not return -1
//****execl (*path, *arg) execute a file 
	}else{
		printf("Failed to fork a new process... \n");
	}



	if(pid != -1)
	{
		for(i=0;i<20;++i)
		{
			sem_wait(sem);//**decrements(locks) semaphore pointed by ()
			printf("A:%s\n",memLocation);
			fflush(stdout);
			sprintf(memLocation,"SHARED %d",i);
			sem_post(sem);
		}
	}
	sem_close(sem);
	s_return = sem_unlink("shared_memory");//****removes semaphore named ()
	if (s_return == -1)
	{
		printf("a: %s\n", strerror( errno ));
		fflush(stdout);
	}
	

	munmap(memLocation,SIZE);
	shm_unlink("shared_memory");
	
	return 0;
}	
